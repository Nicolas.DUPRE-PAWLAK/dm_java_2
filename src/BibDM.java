import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste) {
        if(liste.size() == 0) return null;
        Integer mini = liste.get(0);
        for(int i = 1;i < liste.size(); ++i){
            if(liste.get(i) < liste.get(0)){
                mini = liste.get(i);
            }
        }
        return mini;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean plusPt = true;
        int i = 0;
        while(plusPt && i < liste.size()){
            if (liste.get(i).compareTo(valeur) <= 0) {
                plusPt = false;
            }
            ++i;
        }
        return plusPt;
    }


    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2) {
        List<T> liste3 = new ArrayList<>();
        int i1 = 0;
        int i2 = 0;
        while (i1 < liste1.size() && i2 < liste2.size()) {
            if (liste1.get(i1).compareTo(liste2.get(i2)) < 0) {
                ++i1;
            }
            else if (liste1.get(i1).compareTo(liste2.get(i2)) > 0) {
                ++i2;
            }
            else{
                if(!liste3.contains(liste1.get(i1)))
                    liste3.add(liste1.get(i1));
                ++i1;
                ++i2;
            }
        }
        return liste3;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> lesMots = new ArrayList<>();
        if(texte.length()==0) return lesMots;
        String mot = "";
        for(int i = 0; i < texte.length(); ++i){
            if(texte.charAt(i) == ' '){
                if(mot.length()!=0){
                    lesMots.add(mot);
                    mot = "";
                }
            }
            else{
                mot+=texte.charAt(i);
            }

            // Si le dernier mot
            if(i == texte.length()-1 && mot.length() != 0) lesMots.add(mot);
        }
        return lesMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.length() == 0) return null;
        Map<String, Integer> lesMots = new HashMap<>();
        int value = 0;
        // Etablissement du nombre de répétition de chaque mot
        for(String mot : decoupe(texte)){
            if(lesMots.containsKey(mot)){
                value = lesMots.get(mot);
                lesMots.put(mot, value+1);
            }
            else{
                lesMots.put(mot, 1);
            }
        }
        // Mot le plus présent
        String plusPresent = "";
        for(String mot : lesMots.keySet()){
            if(plusPresent == "") plusPresent = mot;
            else if(lesMots.get(plusPresent) <= lesMots.get(mot)){
                if(lesMots.get(plusPresent) == lesMots.get(mot)){
                    if(plusPresent.compareTo(mot) > 0) plusPresent = mot;
                }
                else{
                    plusPresent = mot;
                }
            }
        }
        return plusPresent;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if(chaine.length() == 0) return true;
        boolean bienP = true;
        int cptOuvrante = 0;
        int cptFermante = 0;
        for(int i = 0; i<chaine.length(); ++i){
            if(chaine.charAt(i) == '(') ++cptOuvrante;
            if(chaine.charAt(i) == ')') ++cptFermante;
        }
        if(cptOuvrante!=cptFermante) bienP = false;
        else if (chaine.charAt(0) != '(') bienP = false;
        else if (chaine.charAt(chaine.length()-1) != ')') bienP = false;
        return bienP;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if(chaine.length() == 0) return true;
        boolean bienP = true;
        char stockOuvrante = ' ';
        int cptOuvrante = 0;
        int cptFermante = 0;
        for(int i = 0; i<chaine.length(); ++i){
            if(chaine.charAt(i) == '(' || chaine.charAt(i) == '['){
                ++cptOuvrante;
                stockOuvrante = chaine.charAt(i);
            }
            if(chaine.charAt(i) == ')' || chaine.charAt(i) == ']') {
                ++cptFermante;
                if (stockOuvrante == '(') {
                    if(chaine.charAt(i) == ']') return false;
                    else stockOuvrante = ' ';
                }
                if(stockOuvrante == '['){
                    if(chaine.charAt(i) == ')') return false;
                    else stockOuvrante = ' ';
                }
            }
        }
        if(cptOuvrante!=cptFermante) bienP = false;
        else if (chaine.charAt(0) != '(' && chaine.charAt(0) != '[') bienP = false;
        else if (chaine.charAt(chaine.length()-1) != ')' && chaine.charAt(chaine.length()-1) != ']') bienP = false;
        return bienP;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size() == 0) return false;
        int bas = 0;
        int haut = liste.size()-1;
        int milieu = 0;
        while(haut - bas > 1){
            milieu = (bas+haut)/2;
            if(liste.get(milieu) == valeur || liste.get(bas) == valeur || liste.get(haut) == valeur) return true;
            else if(liste.get(milieu) > valeur){
                haut = milieu;
            }
            else{
                bas = milieu;
            }
        }
        return false;
    }



}
